package View;

import Model.Board;
import Model.Piece;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class GameView extends Application {
    private Board board;
    private GameController gameController;


    @Override
    public void start(Stage primaryStage) {
        // Inicjalizacja Board i GameController bez wzajemnego ustawiania w konstruktorach
        board = new Board(null); // tymczasowo przekazujemy null
        gameController = new GameController(board); // tymczasowo przekazujemy null

        // Ustawienie wzajemnych referencji
        board.setGameController(gameController);
        gameController.setBoard(board);
        gameController.setGameView(this);

        // Inicjalizacja planszy i ustawienia przeciągania i upuszczania
        board.initializeBoard();

        // Iteracja przez wszystkie pionki na planszy i ustawienie dla nich przeciągania i upuszczania
        for (int row = 0; row < Board.SIZE; row++) {
            for (int col = 0; col < Board.SIZE; col++) {
                Piece piece = board.getBoardArray()[row][col];
                if (piece != null) {
                    gameController.setupDragAndDrop(piece);
                }
            }
        }

        Pane root = new Pane();
        root.getChildren().add(board.getGrid());

        Scene scene = new Scene(root, 400, 400); // Ustaw rozmiar okna
        primaryStage.setTitle("Warcaby");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

