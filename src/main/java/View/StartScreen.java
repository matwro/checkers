package View;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class StartScreen extends Application {

    @Override
    public void start(Stage primaryStage) {
        VBox root = new VBox(20); // Stwórz VBox z odstępem między elementami równym 20
        root.setAlignment(Pos.CENTER); // Wyśrodkuj elementy w VBox

        Button startGameButton = new Button("Start Game");
        startGameButton.setOnAction(e -> {
            GameView gameView = new GameView();
            try {
                gameView.start(new Stage()); // Otwórz nowe okno z grą
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            primaryStage.close(); // Zamknij ekran startowy
        });

        Button exitButton = new Button("Exit");
        exitButton.setOnAction(e -> System.exit(0)); // Zamknij aplikację

        root.getChildren().addAll(startGameButton, exitButton); // Dodaj przyciski do VBox

        Scene scene = new Scene(root, 300, 200); // Stwórz scenę z VBox
        primaryStage.setTitle("Start Screen"); // Ustaw tytuł okna
        primaryStage.setScene(scene); // Ustaw scenę na podstawie VBox
        primaryStage.show(); // Wyświetl okno
    }

    // Załóżmy, że ta metoda jest częścią klasy kontrolującej grę lub głównej klasy aplikacji
    public void showGameOverDialog(String winner, Stage primaryStage) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Koniec Gry");
        alert.setHeaderText("Gra zakończona");
        alert.setContentText("Zwycięzca: " + winner);

        ButtonType restartButton = new ButtonType("Uruchom grę od nowa");
        ButtonType exitButton = new ButtonType("Wyjdź");

        alert.getButtonTypes().setAll(restartButton, exitButton);

        alert.showAndWait().ifPresent(response -> {
            if (response == restartButton) {
                // Restartowanie gry
                GameView gameView = new GameView();
                try {
                    // Zamknij obecne okno
                    primaryStage.close();
                    // Uruchom nową instancję gry
                    gameView.start(new Stage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (response == exitButton) {
                // Wyjście z aplikacji
                System.exit(0);
            }
        });
    }


    public static void main(String[] args) {
        launch(args);
    }
}
