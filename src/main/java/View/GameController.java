package View;

import Checkers.Ai.CheckersBot;
import Model.Board;
import Model.King;
import Model.Piece;
import javafx.animation.PauseTransition;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class GameController {

    private GameView gameView; // Referencja do UI

    public void setGameView(GameView gameView) {
        this.gameView = gameView;
    }
    private Board board;
    private CheckersBot bot;
    private static final int SQUARE_SIZE = 50;

    public boolean gameActive =true;

    public GameController(Board board) {

        this.board = board;
        this.bot = new CheckersBot( board, this);
    }




    public void setBoard(Board board) {
        this.board = board;
    }



    public void setupDragAndDrop(Piece piece) {
        piece.getCircle().setOnDragDetected(event -> {
//            System.out.println(piece.isKing() ? "Jestem królem." : "Jestem pionkiem.");
            Dragboard db = piece.getCircle().startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();
            // Umieszczamy identyfikujące informacje o pionku w Dragboard
            content.putString(piece.getRow() + "," + piece.getColumn()); // Używamy współrzędnych jako identyfikatora
            db.setContent(content);
            event.consume();
        });


        // Ustawienie zdarzenia przeciągania nad planszą
        board.getGrid().setOnDragOver(event -> {
            if (event.getDragboard().hasString()) {
                event.acceptTransferModes(TransferMode.MOVE);
            }
            event.consume();
        });

        // Ustawienie zdarzenia upuszczenia pionka na planszy
        board.getGrid().setOnDragDropped(event -> {
            Dragboard db = event.getDragboard();
            boolean success = false;
            if (db.hasString()) {
                String[] pieceCoordinates = db.getString().split(",");
                int oldRow = Integer.parseInt(pieceCoordinates[0]);
                int oldColumn = Integer.parseInt(pieceCoordinates[1]);

                int newRow = (int) (event.getY() / SQUARE_SIZE);
                int newColumn = (int) (event.getX() / SQUARE_SIZE);
//                System.out.println("Przeciągnięto na: (" + newRow + "," + newColumn + ")");

                // Używamy współrzędnych z Dragboard do zidentyfikowania przeciąganego pionka
                Piece targetPiece = board.getBoardArray()[oldRow][oldColumn];
                if (targetPiece != null) { // Upewniamy się, że znaleźliśmy pionka
                    movePiece(targetPiece, newRow, newColumn); // Teraz wywołujemy movePiece z konkretnym pionkiem
                    success = true;
                }
            }
            event.setDropCompleted(success);
            event.consume();
        });
    }

    public void movePiece(Piece piece, int newRow, int newColumn) {

        if (!gameActive) {
            System.out.println("Gra została zakończona. Żadne więcej ruchy nie są możliwe.");
            return; // Zakończ metodę, jeśli gra nie jest już aktywna
        }
        // Sprawdzenie, czy pole docelowe jest puste
        if (board.getBoardArray()[newRow][newColumn] != null) {
            System.out.println("Ruch jest nieprawidłowy, pole docelowe jest zajęte.");
            return; // Zakończ metodę, jeśli pole docelowe jest zajęte
        }


        if (piece.getColor() != currentPlayerTurn) {
            System.out.println("Nie jest teraz Twoja tura!");
            return;
        }

        if ((newRow + newColumn) % 2 == 0) {
            System.out.println("Ruch na białe pola jest zabroniony.");
            return;
        }

        Piece[][] boardArray = board.getBoardArray();


        boolean wasJump = piece.isKing() ? wasKingJump(piece, piece.getRow(), piece.getColumn(), newRow, newColumn)
                : wasJump(piece, piece.getRow(), piece.getColumn(), newRow, newColumn, boardArray);;
        boolean isValidMove = false;

        if (!wasJump && isJumpAvailableForColor(currentPlayerTurn)) {
            System.out.println("Istnieje możliwość wykonania bicia. Musisz wykonać dostępne bicie.");
            return;
        }

        if (piece.isKing()) {
            isValidMove = checkKingMove(piece, newRow, newColumn) || wasJump;
            if (isValidMove && wasJump) {
                // Dla króla wykonujemy specjalne bicie
                executeKingJump(piece, newRow, newColumn);
            }
        } else { // Sprawdzenie ruchu dla zwykłego pionka
            isValidMove = checkRegularMove(piece, newRow, newColumn) || wasJump;
            if (isValidMove && wasJump) {
                // Dla zwykłego pionka wykonujemy standardowe bicie
                executeJump(piece, newRow, newColumn);
            }
        }

        if (isValidMove) {
            // Aktualizacja stanu gry i interfejsu użytkownika po wykonaniu ruchu
            updateGameStateAndUI(piece, newRow, newColumn);

            // Obsługa promocji pionka na króla
            checkForPromotion(piece, newRow, newColumn);

            // Obsługa kolejnego bicia lub zmiany tury
            handlePostMove(piece, newRow, newColumn, wasJump);
            if (checkIfGameEnded()) {
                gameActive = false;
                System.out.println("Gra zakończona. Gratulacje dla zwycięzcy!");
                return; // Zakończ dalsze ruchy
            }
        } else {
            System.out.println("Nieprawidłowy ruch.");
        }
    }
    private boolean wasJump(Piece piece, int oldRow, int oldColumn, int newRow, int newColumn, Piece[][] boardArray) {
        if (Math.abs(newRow - oldRow) == 2 && Math.abs(newColumn - oldColumn) == 2) {
            int middleRow = (oldRow + newRow) / 2;
            int middleColumn = (oldColumn + newColumn) / 2;
            Piece middlePiece = boardArray[middleRow][middleColumn];

            // Sprawdź, czy na środkowym polu jest pionek przeciwnika i czy pole docelowe jest puste
            if (middlePiece != null && middlePiece.getColor() != piece.getColor() && boardArray[newRow][newColumn] == null) {
                // Dodatkowe sprawdzenie kierunku bicia dla zwykłych pionków
                if (!piece.isKing()) {
                    if (piece.getColor().equals(Color.BLACK) && newRow < oldRow) {
                        return true; // Białe pionki mogą bić tylko do przodu (w dół planszy)
                    } else if (piece.getColor().equals(Color.WHITE) && newRow > oldRow) {
                        return true; // Czarne pionki mogą bić tylko do przodu (w górę planszy)
                    }
                    return false; // Zwykłe pionki próbujące bić do tyłu
                } else {
                    return true; // Króle mogą bić w dowolnym kierunku
                }
            }
        }
        return false; // Nie jest to legalne bicie
    }
    private boolean wasKingJump(Piece king, int oldRow, int oldColumn, int newRow, int newColumn) {
        int rowDirection = Integer.compare(newRow - oldRow, 0);
        int colDirection = Integer.compare(newColumn - oldColumn, 0);
        int steps = Math.max(Math.abs(newRow - oldRow), Math.abs(newColumn - oldColumn));

        boolean opponentPieceEncountered = false;

        // Iteracja przez pola między starą a nową pozycją króla
        for (int step = 1; step <= steps; step++) {
            int checkRow = oldRow + step * rowDirection;
            int checkColumn = oldColumn + step * colDirection;
            Piece pieceAtPosition = board.getBoardArray()[checkRow][checkColumn];

            // Jeśli napotkano pionek przeciwnika
            if (pieceAtPosition != null && pieceAtPosition.getColor() != king.getColor()) {
                if (opponentPieceEncountered) {
                    // Napotkano drugi pionek przeciwnika - ruch niemożliwy
                    return false;
                }
                opponentPieceEncountered = true;
                // Nie kończymy sprawdzania po napotkaniu pierwszego pionka przeciwnika,
                // ponieważ król może kontynuować ruch.
            }

            // Jeśli napotkano własny pionek - ruch niemożliwy
            if (pieceAtPosition != null && pieceAtPosition.getColor() == king.getColor()) {
                return false;
            }
        }

        // Ruch jest możliwy do wykonania, jeśli napotkano pionka przeciwnika
        return opponentPieceEncountered;
    }

    public void updateGameStateAndUI(Piece piece, int newRow, int newColumn) {
        GridPane grid = board.getGrid();
        Piece[][] boardArray = board.getBoardArray();
        int oldRow = piece.getRow();
        int oldColumn = piece.getColumn();

        // Usuń graficzną reprezentację pionka z poprzedniej pozycji
        grid.getChildren().remove(piece.getCircle());

        // Przesuń pionek na nową pozycję w tablicy stanu gry
        boardArray[oldRow][oldColumn] = null;
        boardArray[newRow][newColumn] = piece;

        // Aktualizuj współrzędne pionka
        piece.setRow(newRow);
        piece.setColumn(newColumn);

        // Dodaj graficzną reprezentację pionka na nową pozycję w GridPane
        grid.add(piece.getCircle(), newColumn, newRow);
    }

    private boolean checkRegularMove(Piece piece, int newRow, int newColumn) {
        Piece[][] boardArray = board.getBoardArray();
        int oldRow = piece.getRow();
        int oldColumn = piece.getColumn();

        // Sprawdzenie, czy ruch jest do przodu o jedno pole po przekątnej i czy pole docelowe nie jest zajęte
        if ((piece.getColor().equals(Color.BLACK) && oldRow > newRow || piece.getColor().equals(Color.WHITE) && oldRow < newRow)
                && Math.abs(oldRow - newRow) == 1
                && Math.abs(oldColumn - newColumn) == 1
                && boardArray[newRow][newColumn] == null) {
            return true;
        }
        return false;
    }
    private void executeJump(Piece piece, int newRow, int newColumn) {
        GridPane grid = board.getGrid();
        Piece[][] boardArray = board.getBoardArray();
        int oldRow = piece.getRow();
        int oldColumn = piece.getColumn();

        // Obliczanie pozycji zbitego pionka
        int middleRow = (oldRow + newRow) / 2;
        int middleColumn = (oldColumn + newColumn) / 2;

        // Pobieranie zbitego pionka
        Piece zbityPionek = boardArray[middleRow][middleColumn];

        if (zbityPionek != null) {
            // Usuwanie zbitego pionka z planszy
            grid.getChildren().remove(zbityPionek.getCircle());

            // Usuwanie zbitego pionka z tablicy stanu gry
            boardArray[middleRow][middleColumn] = null;
        }

        // Kontynuacja aktualizacji stanu gry i UI dla pionka wykonującego ruch
        updateGameStateAndUI(piece, newRow, newColumn);
    }

    private void checkForPromotion(Piece piece, int newRow, int newColumn) {
        // Sprawdzenie, czy pionek osiągnął przeciwny koniec planszy i nie jest jeszcze królem
        if (!piece.isKing() && ((piece.getColor().equals(Color.BLACK) && newRow == 0) ||
                (piece.getColor().equals(Color.WHITE) && newRow == board.getBoardArray().length - 1))) {
            System.out.println("Pionek promowany na króla");
            promoteToKing(piece, newRow, newColumn);
        }
    }





        private void promoteToKing(Piece piece, int row, int column) {
//        System.out.println("Promocja na króla: usuwanie starego pionka");
        GridPane grid = board.getGrid();
        grid.getChildren().remove(piece.getCircle()); // Upewnij się, że to dokładnie ten obiekt, który chcesz usunąć

//        System.out.println("Dodawanie nowego króla do planszy");
        King king = new King(piece.getRadius(), piece.getColor(), row, column);
        board.getBoardArray()[row][column] = king; // Aktualizacja stanu planszy



//        System.out.println("Dodawanie graficznej reprezentacji króla do GridPane");
        grid.add(king.getCircle(), column, row); // Ponowne dodanie do GridPane

        // Ustawienie zdarzeń przeciągania dla króla
        setupDragAndDrop(king);
        updateGameStateAndUI(king, row, column);
    }
    private Color currentPlayerTurn = Color.WHITE;

    private void changeTurn() {
        currentPlayerTurn = (currentPlayerTurn == Color.WHITE) ? Color.BLACK : Color.WHITE;
        System.out.println("Teraz kolej na gracza: " + getColorName(currentPlayerTurn));

        // Jeśli teraz kolej czarnych, automatycznie wykonaj ruch bota
        if (currentPlayerTurn == Color.BLACK) {
            System.out.println("Bot wykonuje ruch...");
            makeMoveForBot();
            changeTurn(); // zmień turę po wykonaniu ruchu przez bota
        }
    }
    private void handlePostMove(Piece piece, int newRow, int newColumn, boolean wasJump) {
        boolean canContinueJumping = false;

        if (wasJump) {
            if (piece.isKing()) {
                canContinueJumping = canKingMakeAnotherJump(piece);
            } else {
                canContinueJumping = canMakeAnotherJump(piece, newRow, newColumn);
            }

            if (canContinueJumping) {
                System.out.println("Możliwe jest kolejne bicie. Gracz kontynuuje.");
            } else {
                System.out.println("Brak możliwości kolejnego bicia. Zmiana tury.");
                changeTurn(); // Zmiana tury
            }
        } else {
            System.out.println("Brak możliwości kolejnego bicia. Zmiana tury.");
            changeTurn(); // Zmiana tury
        }
    }

    private String getColorName(Color color) {
        if (color.equals(Color.WHITE)) {
            return "Biały";
        } else if (color.equals(Color.BLACK)) {
            return "Czarny";
        } else {
            return "Nieznany kolor";
        }
    }
    private boolean canMakeAnotherJump(Piece piece, int row, int column) {
        int[] directionRow = piece.isKing() ? new int[]{1, 1, -1, -1} : piece.getColor().equals(Color.BLACK) ? new int[]{-1, -1} : new int[]{1, 1};
        int[] directionCol = piece.isKing() ? new int[]{1, -1, 1, -1} : new int[]{1, -1};

        for (int i = 0; i < directionRow.length; i++) {
            int enemyRow = row + directionRow[i];
            int enemyCol = column + directionCol[i];
            int landingRow = enemyRow + directionRow[i];
            int landingCol = enemyCol + directionCol[i];

            // Sprawdź, czy pole docelowe znajduje się w granicach planszy
            if (landingRow >= 0 && landingRow < board.getBoardArray().length &&
                    landingCol >= 0 && landingCol < board.getBoardArray()[0].length) {
                Piece enemyPiece = board.getBoardArray()[enemyRow][enemyCol];
                Piece landingSpot = board.getBoardArray()[landingRow][landingCol];

                // Sprawdź, czy istnieje pionek przeciwnika do zbicia i czy pole za nim jest puste
                if (enemyPiece != null && landingSpot == null && enemyPiece.getColor() != piece.getColor()) {
                    return true; // Możliwe jest kolejne bicie
                }
            }
        }
        return false; // Nie ma możliwości kolejnego bicia
    }
    private boolean canKingMakeAnotherJump(Piece king) {
        int[][] directions = {{-1, -1}, {-1, 1}, {1, -1}, {1, 1}}; // Reprezentacja czterech kierunków na ukos
        int currentRow = king.getRow();
        int currentColumn = king.getColumn();

        for (int[] direction : directions) {
            int rowDirection = direction[0];
            int colDirection = direction[1];

            int nextRow = currentRow + rowDirection;
            int nextCol = currentColumn + colDirection;

            // Sprawdzanie każdego pola wzdłuż kierunku aż do napotkania pionka lub końca planszy
            while (nextRow >= 0 && nextRow < Board.SIZE && nextCol >= 0 && nextCol < Board.SIZE) {
                Piece nextPiece = board.getBoardArray()[nextRow][nextCol];

                // Jeśli napotkano pionka przeciwnika, sprawdź, czy za nim jest wolne pole
                if (nextPiece != null && nextPiece.getColor() != king.getColor()) {
                    int jumpOverRow = nextRow + rowDirection;
                    int jumpOverCol = nextCol + colDirection;

                    // Sprawdzenie, czy pole za pionkiem przeciwnika jest wolne
                    if (jumpOverRow >= 0 && jumpOverRow < Board.SIZE && jumpOverCol >= 0 && jumpOverCol < Board.SIZE
                            && board.getBoardArray()[jumpOverRow][jumpOverCol] == null) {
                        return true; // Znaleziono możliwe bicie
                    } else {
                        break; // Napotkano przeszkodę lub koniec planszy za pionkiem przeciwnika
                    }
                } else if (nextPiece != null) {
                    break; // Napotkano własny pionek lub koniec możliwego ruchu
                }

                nextRow += rowDirection;
                nextCol += colDirection;
            }

        }

        return false; // Nie znaleziono możliwych bić
    }
    private boolean checkKingMove(Piece king, int newRow, int newColumn) {
        int currentRow = king.getRow();
        int currentColumn = king.getColumn();
        int rowDirection = Integer.compare(newRow - currentRow, 0);
        int colDirection = Integer.compare(newColumn - currentColumn, 0);

        int distance = Math.abs(newRow - currentRow);
        if (distance != Math.abs(newColumn - currentColumn)) {
            return false; // Ruch nie jest po przekątnej
        }

        boolean opponentPieceEncountered = false;

        // Nowa weryfikacja: upewnienie się, że pole docelowe jest puste
        Piece finalPositionPiece = board.getBoardArray()[newRow][newColumn];
        if (finalPositionPiece != null) {
            return false; // Pole docelowe nie jest puste, ruch nieprawidłowy
        }

        for (int step = 1; step < distance; step++) {
            int checkRow = currentRow + step * rowDirection;
            int checkColumn = currentColumn + step * colDirection;
            Piece pieceAtPosition = board.getBoardArray()[checkRow][checkColumn];

            if (pieceAtPosition != null) {
                if (pieceAtPosition.getColor() == king.getColor()) {
                    return false; // Napotkano pionka tego samego koloru, ruch nieprawidłowy
                } else if (opponentPieceEncountered) {
                    return false; // Napotkano więcej niż jednego pionka przeciwnika na drodze, ruch nieprawidłowy
                } else {
                    opponentPieceEncountered = true; // Pierwszy napotkany pionek przeciwnika
                }
            }
        }

        // Ruch jest prawidłowy jeśli nie napotkano pionka tego samego koloru i pole docelowe jest puste
        // oraz jeśli napotkano pionka przeciwnika to tylko jeden na drodze
        return true;
    }
    private void executeKingJump(Piece king, int newRow, int newColumn) {
//        System.out.println("Executing king jump");
        int currentRow = king.getRow();
        int currentColumn = king.getColumn();
        int rowDirection = Integer.compare(newRow - currentRow, 0);
        int colDirection = Integer.compare(newColumn - currentColumn, 0);

        boolean opponentPieceEncountered = false;

        for (int step = 1; step < Math.max(Math.abs(newRow - currentRow), Math.abs(newColumn - currentColumn)); step++) {
            int checkRow = currentRow + step * rowDirection;
            int checkColumn = currentColumn + step * colDirection;
            Piece pieceAtPosition = board.getBoardArray()[checkRow][checkColumn];

            if (pieceAtPosition != null && pieceAtPosition.getColor() != king.getColor() && !opponentPieceEncountered) {

                // Usuń graficzną reprezentację pionka z planszy i z tablicy stanu gry
                board.getGrid().getChildren().remove(pieceAtPosition.getCircle());

                board.getBoardArray()[checkRow][checkColumn] = null;
                opponentPieceEncountered = true; // Zaznacz, że pionek został zbity

                board.getGrid().getChildren().remove(pieceAtPosition.getCircle());

            } else if (pieceAtPosition != null || opponentPieceEncountered) {
                break; // Przerwij pętlę po zbiciu pionka lub napotkaniu innego pionka
            }
        }

        // Przesuń króla na nową pozycję i aktualizuj UI oraz stan planszy
        board.getBoardArray()[currentRow][currentColumn] = null;
        board.getBoardArray()[newRow][newColumn] = king;
        updateGameStateAndUI(king, newRow, newColumn);
    }
    public boolean isJumpAvailableForColor(Color playerColor) {
        for (int row = 0; row < Board.SIZE; row++) {
            for (int col = 0; col < Board.SIZE; col++) {
                Piece piece = board.getBoardArray()[row][col];
                if (piece != null && piece.getColor() == playerColor) {
                    if (piece.isKing()) {
                        if (isKingJumpAvailable(piece, row, col)) {
                            return true;
                        }
                    } else {
                        if (isRegularJumpAvailable(piece, row, col)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean isRegularJumpAvailable(Piece piece, int row, int col) {
        int rowDirection = piece.getColor() == Color.BLACK ? -1 : 1; // Dla białych w górę planszy, dla czarnych w dół
        int[] colDirections = {-1, 1}; // Sprawdzanie bicia w lewo i prawo
        Color opponentColor = piece.getColor() == Color.WHITE ? Color.BLACK : Color.WHITE;

        for (int colDirection : colDirections) {
            int opponentRow = row + rowDirection; // Sprawdzamy pole bezpośrednio "przed" pionkiem
            int opponentCol = col + colDirection;
            int jumpRow = opponentRow + rowDirection; // Pole za potencjalnie zbitym pionkiem
            int jumpCol = opponentCol + colDirection;

            // Sprawdzenie, czy nie wychodzimy poza planszę
            if (isWithinBoard(opponentRow, opponentCol) && isWithinBoard(jumpRow, jumpCol)) {
                Piece opponentPiece = board.getBoardArray()[opponentRow][opponentCol];
                Piece jumpSpace = board.getBoardArray()[jumpRow][jumpCol];

                // Sprawdzenie, czy istnieje pionek przeciwnika do zbicia i czy pole za nim jest wolne
                if (opponentPiece != null && opponentPiece.getColor() == opponentColor && jumpSpace == null) {
                    return true; // Istnieje możliwość bicia
                }
            }
        }
        return false; // Brak możliwości bicia
    }

    private boolean isKingJumpAvailable(Piece king, int row, int col) {
        int[][] directions = {{-1, -1}, {-1, 1}, {1, -1}, {1, 1}}; // Możliwe kierunki ruchu na ukos

        for (int[] direction : directions) {
            int checkRow = king.getRow() + direction[0];
            int checkCol = king.getColumn() + direction[1];
            boolean opponentPieceEncountered = false;

            while (checkRow >= 0 && checkRow < Board.SIZE && checkCol >= 0 && checkCol < Board.SIZE) {
                Piece pieceAtPosition = board.getBoardArray()[checkRow][checkCol];

                // Jeśli napotkano pionka przeciwnika
                if (pieceAtPosition != null && pieceAtPosition.getColor() != king.getColor()) {
                    opponentPieceEncountered = true;
                    // Kontynuujemy sprawdzanie, czy za pionkiem przeciwnika jest wolne pole
                } else if (opponentPieceEncountered && pieceAtPosition == null) {
                    // Napotkano wolne pole za pionkiem przeciwnika, bicie jest możliwe
                    return true;
                } else if (pieceAtPosition != null) {
                    // Napotkano pionka (swojego lub przeciwnika) przed znalezieniem wolnego pola, kończymy sprawdzanie tej ścieżki
                    break;
                }

                // Przesuwanie się dalej wzdłuż kierunku
                checkRow += direction[0];
                checkCol += direction[1];
            }
        }

        return false; // Nie znaleziono możliwości bicia
    }
    private boolean isWithinBoard(int row, int col) {
        return row >= 0 && row < Board.SIZE && col >= 0 && col < Board.SIZE;
    }
    public boolean checkIfGameEnded() {
        boolean whitePiecesLeft = false;
        boolean blackPiecesLeft = false;

        Piece[][] boardArray = board.getBoardArray(); // Zakładam, że istnieje taka metoda

        // Przejście przez wszystkie pola na planszy
        for (int row = 0; row < Board.SIZE; row++) {
            for (int col = 0; col < Board.SIZE; col++) {
                Piece piece = boardArray[row][col];
                if (piece != null) {
                    if (piece.getColor() == Color.WHITE) {
                        whitePiecesLeft = true;
                    } else if (piece.getColor() == Color.BLACK) {
                        blackPiecesLeft = true;
                    }
                }
            }
        }

        // Sprawdzenie, czy którykolwiek z graczy nie ma już pionków
        if (!whitePiecesLeft || !blackPiecesLeft) {
            // Wyświetl komunikat o zakończeniu gry i zwycięzcy
            String winner = whitePiecesLeft ? "Białe" : "Czarne";
            System.out.println("Gra zakończona. Zwycięzca: " + winner);
            return true; // Gra została zakończona
        }

        return false; // Gra kontynuowana
    }


    private List<int[]> getPossibleJumpsForPiece(Piece piece) {
        List<int[]> jumps = new ArrayList<>();
        // Dla króla sprawdzamy wszystkie kierunki, dla zwykłego pionka tylko do przodu
        int[][] directions = piece.isKing() ?
                new int[][]{{-1, -1}, {-1, 1}, {1, -1}, {1, 1}} :
                piece.getColor() == Color.BLACK ?
                        new int[][]{{-1, -1}, {-1, 1}} : // Dla czarnych pionków
                        new int[][]{{1, -1}, {1, 1}};   // Dla białych pionków

        for (int[] direction : directions) {
            if (piece.isKing()) {
                // Dla króla sprawdzamy możliwość bicia przez więcej pól
                for (int i = 1; isWithinBoard(piece.getRow() + i * direction[0], piece.getColumn() + i * direction[1]); i++) {
                    int nextRow = piece.getRow() + i * direction[0];
                    int nextCol = piece.getColumn() + i * direction[1];
                    Piece nextPiece = board.getBoardArray()[nextRow][nextCol];
                    // Jeśli natrafimy na pionek przeciwnika, sprawdzamy możliwość bicia
                    if (nextPiece != null && nextPiece.getColor() != piece.getColor()) {
                        int jumpOverRow = nextRow + direction[0];
                        int jumpOverCol = nextCol + direction[1];
                        if (isWithinBoard(jumpOverRow, jumpOverCol) && board.getBoardArray()[jumpOverRow][jumpOverCol] == null) {
                            jumps.add(new int[]{piece.getRow(), piece.getColumn(), jumpOverRow, jumpOverCol});
                            break; // Znaleziono bicie, nie trzeba dalej sprawdzać w tym kierunku
                        }
                    }
                    if (nextPiece != null) {
                        break; // Napotkano pionek, kończymy sprawdzanie w tym kierunku
                    }
                }
            } else {
                // Dla zwykłego pionka, sprawdzamy tylko możliwość bicia na bezpośrednio przyległe pola
                int potentialRow = piece.getRow() + 2 * direction[0];
                int potentialCol = piece.getColumn() + 2 * direction[1];
                int middleRow = piece.getRow() + direction[0];
                int middleCol = piece.getColumn() + direction[1];
                if (isWithinBoard(potentialRow, potentialCol) && isWithinBoard(middleRow, middleCol)) {
                    Piece potentialPiece = board.getBoardArray()[potentialRow][potentialCol];
                    Piece middlePiece = board.getBoardArray()[middleRow][middleCol];
                    if (potentialPiece == null && middlePiece != null && middlePiece.getColor() != piece.getColor()) {
                        jumps.add(new int[]{piece.getRow(), piece.getColumn(), potentialRow, potentialCol});
                    }
                }
            }
        }
        return jumps;
    }

    private List<int[]> getPossibleRegularMovesForPiece(Piece piece) {
        List<int[]> moves = new ArrayList<>();
        // Dla króla sprawdzamy ruchy na całej długości przekątnych
        if (piece.isKing()) {
            int[][] directions = {{-1, -1}, {-1, 1}, {1, -1}, {1, 1}};
            for (int[] direction : directions) {
                for (int i = 1; i < Board.SIZE; i++) {
                    int potentialRow = piece.getRow() + i * direction[0];
                    int potentialCol = piece.getColumn() + i * direction[1];
                    if (!isWithinBoard(potentialRow, potentialCol)) break; // Przerwij, jeśli wyjdziesz poza planszę
                    Piece potentialPiece = board.getBoardArray()[potentialRow][potentialCol];
                    if (potentialPiece == null) {
                        moves.add(new int[]{piece.getRow(), piece.getColumn(), potentialRow, potentialCol});
                    } else {
                        break; // Przerwij, jeśli napotkasz innego pionka
                    }
                }
            }
        } else {
            // Dla zwykłego pionka tylko ruchy do przodu o jedno pole
            int[][] directions = piece.getColor() == Color.BLACK ? new int[][]{{-1, -1}, {-1, 1}} : new int[][]{{1, -1}, {1, 1}};
            for (int[] direction : directions) {
                int potentialRow = piece.getRow() + direction[0];
                int potentialCol = piece.getColumn() + direction[1];
                if (isWithinBoard(potentialRow, potentialCol)) {
                    Piece potentialPiece = board.getBoardArray()[potentialRow][potentialCol];
                    if (potentialPiece == null) {
                        moves.add(new int[]{piece.getRow(), piece.getColumn(), potentialRow, potentialCol});
                    }
                }
            }
        }
        return moves;
    }
    public List<Piece> getPiecesForColor(Color color) {
        List<Piece> pieces = new ArrayList<>();
        for (int row = 0; row < Board.SIZE; row++) {
            for (int col = 0; col < Board.SIZE; col++) {
                Piece piece = board.getBoardArray()[row][col];
                if (piece != null && piece.getColor().equals(color)) {
                    pieces.add(piece);
                }
            }
        }
        return pieces;
    }
    private void executeMoveDirectly(Piece piece, int newRow, int newColumn) {
        // Usunięcie pionka z obecnej pozycji
        board.getBoardArray()[piece.getRow()][piece.getColumn()] = null;
        // Przesunięcie pionka na nowe współrzędne
        piece.setRow(newRow);
        piece.setColumn(newColumn);
        board.getBoardArray()[newRow][newColumn] = piece;
        updateGameStateAndUI(piece, newRow,newColumn);
    }

    public void makeMoveForBot() {
        PauseTransition pause = new PauseTransition(Duration.seconds(1));
        pause.setOnFinished(event -> {
            executeBotMove();
        });
        pause.play();
    }
    private boolean lastMoveWasJump = false;
    private void executeBotMove() {
        List<int[]> allPossibleJumps = new ArrayList<>();
        List<int[]> allPossibleMoves = new ArrayList<>();

        // Pobieranie wszystkich możliwych ruchów dla czarnych pionków
        for (Piece piece : getPiecesForColor(Color.BLACK)) {
            allPossibleJumps.addAll(getPossibleJumpsForPiece(piece));
            allPossibleMoves.addAll(getPossibleRegularMovesForPiece(piece));
        }
//        System.out.println("Możliwe bicia:");
//        for (int[] jump : allPossibleJumps) {
//            System.out.println("Pionek na pozycji: [" + jump[0] + ", " + jump[1] + "] może wykonać bicie na: [" + jump[2] + ", " + jump[3] + "]");
//        }
//        System.out.println("Możliwe ruchy:");
//        for (int[] move : allPossibleMoves) {
//            System.out.println("Pionek na pozycji: [" + move[0] + ", " + move[1] + "] może się przemieścić na: [" + move[2] + ", " + move[3] + "]");
//        }


        Random random = new Random();
        int[] selectedMove;

        // Wybór ruchu z priorytetem dla bicia
        if (!allPossibleJumps.isEmpty()) {
            selectedMove = allPossibleJumps.get(random.nextInt(allPossibleJumps.size()));
        } else if (!allPossibleMoves.isEmpty()) {
            selectedMove = allPossibleMoves.get(random.nextInt(allPossibleMoves.size()));
        } else {
            System.out.println("Bot nie może wykonać żadnego ruchu.");
            return;
        }

        // Znalezienie pionka na podstawie początkowych współrzędnych ruchu
        Piece pieceToMove = findPieceAtPosition(selectedMove[0], selectedMove[1]);
        if (pieceToMove == null) {
            System.out.println("Błąd: nie znaleziono pionka do wykonania ruchu.");
            return;
        }

        // Sprawdzenie, czy ruch jest biciem
        boolean isJumpMove = pieceToMove.isKing() ? wasKingJump(pieceToMove, pieceToMove.getRow(), pieceToMove.getColumn(), selectedMove[2], selectedMove[3])
                : wasJump(pieceToMove, pieceToMove.getRow(), pieceToMove.getColumn(), selectedMove[2], selectedMove[3], board.getBoardArray());

        if (isJumpMove) {
            // Ruch jest biciem
            if (pieceToMove.isKing()) {
                executeKingJump(pieceToMove, selectedMove[2], selectedMove[3]);
            } else {
                executeJump(pieceToMove, selectedMove[2], selectedMove[3]);
            }
            lastMoveWasJump = true;
        } else {
            // Zwykły ruch
            executeMoveDirectly(pieceToMove, selectedMove[2], selectedMove[3]);
            lastMoveWasJump = false;
        }

        // Sprawdzenie, czy po ruchu istnieje możliwość kolejnego bicia
        if (lastMoveWasJump && canMakeAnotherJump(pieceToMove, selectedMove[2], selectedMove[3])) {
            // Opóźnienie przed kolejnym ruchem
            PauseTransition pauseForNextJump = new PauseTransition(Duration.seconds(1));
            pauseForNextJump.setOnFinished(event -> executeBotMove()); // Rekurencyjne wywołanie tej samej metody dla kolejnego ruchu
            pauseForNextJump.play();
        }
        checkForPromotion(pieceToMove, selectedMove[2], selectedMove[3]);
        if (checkIfGameEnded()) {
            gameActive = false;
            System.out.println("Gra zakończona. Gratulacje dla zwycięzcy!");
            return; // Zakończ dalsze ruchy
        }
    }


    public Piece findPieceAtPosition(int row, int col) {
        // Sprawdzenie, czy współrzędne są w granicach planszy
        if (row >= 0 && row < Board.SIZE && col >= 0 && col < Board.SIZE) {
            return board.getBoardArray()[row][col]; // Zwróć pionek na danych współrzędnych
        } else {
            return null; // Jeśli współrzędne są poza planszą, zwróć null
        }
    }




}