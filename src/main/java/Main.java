import View.StartScreen;

public class Main {
    public static void main(String[] args) {
        StartScreen.launch(StartScreen.class, args);
    }
}