package Model;

import View.GameController;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

public class Board {
    public static final int SIZE = 8;

    private static final int SQUARE_SIZE = 50;

    private Piece[][] board = new Piece[SIZE][SIZE];
    private GridPane grid = new GridPane();

    private GameController gameController;

    public Board(GameController gameController) {
        this.gameController = gameController;
        // reszta inicjalizacji...
    }


    public void initializeBoard() {
        for (int row = 0; row < SIZE; row++) {
            for (int col = 0; col < SIZE; col++) {
                Rectangle square = new Rectangle(SQUARE_SIZE, SQUARE_SIZE);
                if ((row + col) % 2 == 0) {
                    square.setFill(Color.WHITE);
                } else {
                    square.setFill(Color.GREY);
                }
                grid.add(square, col, row); // Dodaj kwadrat do planszy

                if ((row + col) % 2 != 0) {
                    Piece piece = null;
                    if (row < 3) {
                        piece = new Piece(SQUARE_SIZE / 2, Color.WHITE, row, col);
                    } else if (row > 4) {
                        piece = new Piece(SQUARE_SIZE / 2, Color.BLACK, row, col);
                    }
                    if (piece != null) {
                        grid.add(piece.getCircle(), col, row); // Dodaj pionek do planszy
                        board[row][col] = piece; // Aktualizuj tablicę pionków
                    }
                }
            }
        }
    }


    public GridPane getGrid() {
        return grid;
    }
    public Piece[][] getBoardArray() {
        return board;
    }
    public void setGameController(GameController gameController) {
        this.gameController = gameController;
    }

}