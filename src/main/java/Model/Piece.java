package Model;

import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;

public class Piece extends StackPane {
    private int row;
    private int column;
    boolean isKing = false;
    private Color color;
    private Circle piece;
    private Polygon kingSymbol;


    public Piece(double radius, Color color, int row, int column) {
        this.row = row;
        this.column = column;
        this.color = color;
        piece = new Circle(radius, color);

        // Tworzenie symbolu damki
        kingSymbol = new Polygon();
        kingSymbol.getPoints().addAll(new Double[]{
                0.0, -radius / 3,
                radius / 3, radius / 3,
                -radius / 3, radius / 3
        });
        kingSymbol.setFill(Color.BLUE);
        kingSymbol.setVisible(false);

        // Dodawanie pionka i symbolu do StackPane
        this.getChildren().addAll(piece, kingSymbol);

        // Ustawienie zdarzeń przeciągania
        setOnDragDetected(event -> {

//            System.out.println("Rozpoczęto przeciąganie pionka");
            Dragboard db = startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();
//            content.putString("Rozpoczęto przeciąganie pionka"+ row + "," + column); // Możesz tu umieścić informacje o pionku
            db.setContent(content);
            event.consume();
        });

        setOnDragDone(event -> {
//            System.out.println("Zakończono przeciąganie pionka");
            event.consume();
        });
    }
    public boolean isKing() {
        return isKing;
    }


    public Color getColor() {
        return (Color) piece.getFill();
    }
    public Circle getCircle() {
        return piece;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public double getRadius() {
        return piece.getRadius();
    }

}