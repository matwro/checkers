package Model;


import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.paint.Color;

public class King extends Piece {
    public King(double radius, Color color, int row, int column) {
        super(radius, color, row, column);
        this.isKing = true;

        setOnDragDetected(event -> {
            System.out.println("Rozpoczęto przeciąganie króla");
            Dragboard db = startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();
            content.putString("Rozpoczęto przeciąganie króla " + row + "," + column); // Informacje o królu
            db.setContent(content);
            event.consume();
        });
        setOnDragDone(event -> {
            System.out.println("Zakończono przeciąganie Króla");
            event.consume();
        });
        getCircle().setStroke(Color.GOLD); // Na przykład, oznaczenie króla
        getCircle().setStrokeWidth(2); // Ustawienie szerokości obramowania
        getCircle().setRadius(getCircle().getRadius() - 2);



    }

}